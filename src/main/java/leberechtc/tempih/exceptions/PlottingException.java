package leberechtc.tempih.exceptions;

public class PlottingException extends RuntimeException {

    public PlottingException(String message) {
        super(message);
    }

    public PlottingException(String message, Throwable cause) {
        super(message, cause);
    }

}

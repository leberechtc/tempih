package leberechtc.tempih;

import leberechtc.tempih.exceptions.SystemException;

import static leberechtc.tempih.TemperatureObserver.TemperatureState.*;

public class TemperatureObserver {

    public enum TemperatureState {
        TOO_HIGH, TOO_LOW, JUST_RIGHT
    }

    private final double minimalTemperature;
    private final double maximalTemperature;

    public TemperatureObserver(double minimalTemperature, double maximalTemperature) {
        if (minimalTemperature > maximalTemperature) {
            throw new SystemException("The minimal temperature in the configuration is higher than the maximal temperature, you might want to check that.");
        }
        this.minimalTemperature = minimalTemperature;
        this.maximalTemperature = maximalTemperature;
    }

    public TemperatureState checkMeasurement(Measurement measurement) {
        if (measurement.getTemperature() > maximalTemperature) {
            return TOO_HIGH;
        }
        if (measurement.getTemperature() < minimalTemperature) {
            return TOO_LOW;
        }
        return JUST_RIGHT;
    }

}

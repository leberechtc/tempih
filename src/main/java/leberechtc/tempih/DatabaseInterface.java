package leberechtc.tempih;

import leberechtc.tempih.exceptions.DatabaseException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

import static leberechtc.tempih.Configuration.*;
import static leberechtc.tempih.SQLStatements.*;

public class DatabaseInterface implements Recorder {

    private final static Logger logger = Logger.getLogger(DatabaseInterface.class);
    private Connection databaseConnection;

    private PreparedStatement addMeasurement;

    public DatabaseInterface() {
        setupDatabase();
        initializeSession();
        prepareStatements();
    }

    private void initializeSession() {
        if (getCurrentSession() == 0) {
            addSession();
        } else {
            continueSession();
        }
    }

    private void addSession() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        logger.debug("Initializing new measurement session.");
        try {
            String sql = String.format(INSERT_NEW_SESSION, "DEFAULT");
            PreparedStatement newSessionStatement = databaseConnection.prepareStatement(sql);
            newSessionStatement.setTimestamp(1, timestamp);
            newSessionStatement.execute();
            // check which session we are actually in
            PreparedStatement currentSessionStatement = databaseConnection.prepareStatement(SESSION_FOR_TIMESTAMP);
            currentSessionStatement.setTimestamp(1, timestamp);
            ResultSet resultSet = currentSessionStatement.executeQuery();
            if (resultSet.next()) {
                Configuration.setCurrentSession(resultSet.getInt(1));
                resultSet.close();
                currentSessionStatement.close();
            } else {
                throw new DatabaseException("Unable to determine new session identifier.");
            }
        } catch (SQLException e) {
            throw new DatabaseException("Unable to create new session.", e);
        }
    }

    private void continueSession() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        logger.debug("Continuing or starting session " + getCurrentSession() + ".");
        try {
            String sql = String.format(INSERT_NEW_SESSION, "?");
            PreparedStatement statement = databaseConnection.prepareStatement(sql);
            statement.setInt(1, getCurrentSession());
            statement.setTimestamp(2, timestamp);
            statement.execute();
        } catch (SQLException e) {
            throw new DatabaseException("Unable to create new session.", e);
        }
    }

    private void prepareStatements() {
        try {
            addMeasurement = databaseConnection.prepareStatement(ADD_MEASUREMENT);
        } catch (SQLException e) {
            throw new DatabaseException("Unable to prepare sql statements.", e);
        }
    }

    public List<Measurement> getMeasurementsFromSession(int session) {
        return null;
    }

    @Override
    public void addMeasurement(Measurement measurement) {
        try {
            addMeasurement.setInt(1, getCurrentSession());
            addMeasurement.setTimestamp(2, measurement.getTimestamp());
            addMeasurement.setDouble(3, measurement.getTemperature());
            addMeasurement.execute();
        } catch (SQLException e) {
            throw new DatabaseException("Unable to prepare sql statements.", e);
        }
    }

    private void setupDatabase() {
        // establish server connection
        Connection serverConnection = createServerConnection();
        // check if database exists
        if (!databaseExists(serverConnection)) {
            // if not create it
            createDatabase(serverConnection);
        } else {
            logger.info("Using existing database " + getDatabaseName() + ".");
        }
        // get connection to database
        databaseConnection = createDatabaseConnection();
        // create table session
        if (!tableExists(databaseConnection, "Session")) {
            logger.debug("Creating table Session");
            try {
                Statement statement = databaseConnection.createStatement();
                statement.executeUpdate(CREATE_TABLE_SESSION);
            } catch (SQLException e) {
                throw new DatabaseException("Unable to create table Session.", e);
            }
        } else {
            logger.debug("Using existing table Session.");
        }
        // create table measurement
        if (!tableExists(databaseConnection, "Measurement")) {
            logger.debug("Creating table Measurement");
            try {
                Statement statement = databaseConnection.createStatement();
                statement.executeUpdate(CREATE_TABLE_MEASUREMENT);
            } catch (SQLException e) {
                throw new DatabaseException("Unable to create table Measurement.", e);
            }
        } else {
            logger.debug("Using existing table Measurement.");
        }
        logger.info("Initialized database successfully.");
    }

    private static Connection createServerConnection() {
        logger.info("Connecting to postgre sql server.");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new DatabaseException("Unable to get postgresql jdbc driver.", e);
        }
        Connection connection;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost/", getDatabaseUserName(), getDatabaseUserPassword());
            return connection;
        } catch (SQLException e) {
            throw new DatabaseException("Unable to connect to database at localhost with the specified credentials.", e);
        }
    }

    private static Connection createDatabaseConnection() {
        Connection connection;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost/" + getDatabaseName(), getDatabaseUserName(), getDatabaseUserPassword());
            return connection;
        } catch (SQLException e) {
            throw new DatabaseException("Unable to connect to database " + getDatabaseName() + " at localhost with the specified credentials.", e);
        }
    }

    private static boolean databaseExists(Connection serverConnection) {
        logger.debug("Checking if database " + getDatabaseName() + " already exists.");
        try {
            Statement statement = serverConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(DATABASE_EXISTS);
            if (resultSet.next()) {
                boolean databaseExists = resultSet.getString(1).equalsIgnoreCase(getDatabaseName());
                resultSet.close();
                statement.close();
                return databaseExists;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DatabaseException("Unable to verify existence of database " + getDatabaseName() + ".");
        }
    }

    private static void createDatabase(Connection serverConnection) {
        logger.info("Creating database " + getDatabaseName());
        try {
            serverConnection.setAutoCommit(true);
            Statement statement = serverConnection.createStatement();
            statement.executeUpdate(CREATE_DATABASE);
        } catch (SQLException e) {
            throw new DatabaseException("Unable to create database " + getDatabaseName() + ".", e);
        }
    }

    private static boolean tableExists(Connection databaseConnection, String tableName) {
        logger.debug("Checking if table " + tableName + " already exists.");
        try {
            Statement statement = databaseConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format(TABLE_EXISTS, tableName));
            if (resultSet.next()) {
                boolean tableExists = resultSet.getBoolean(1);
                resultSet.close();
                statement.close();
                return tableExists;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DatabaseException("Unable to verify existence of table " + tableName + ".");
        }
    }

}

package leberechtc.tempih;

import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.GpioUtil;

public class RelayInterface {

    private final GpioPinDigitalOutput relayPin;

    public RelayInterface() {
        GpioUtil.enableNonPrivilegedAccess();
        final GpioController gpioRelay = GpioFactory.getInstance();
        relayPin = gpioRelay.provisionDigitalOutputPin(RaspiPin.GPIO_01, "RelayHeat", PinState.HIGH);
    }

    public void switchOn() {
        relayPin.low();
    }

    public void switchOff() {
        relayPin.high();
    }

}

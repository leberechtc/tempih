package leberechtc.tempih;

import static leberechtc.tempih.Configuration.getDatabaseName;
import static leberechtc.tempih.Configuration.getDatabaseUserName;

public class SQLStatements {

    public static String DATABASE_EXISTS = "SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('" + getDatabaseName() + "');";
    public static String CREATE_DATABASE = "CREATE DATABASE " + getDatabaseName();

    public static String CREATE_TABLE_SESSION = "CREATE TABLE \"Session\"\n" +
            "(\n" +
            "    \"SessionID\" serial NOT NULL,\n" +
            "    \"StartTimestamp\" timestamp without time zone NOT NULL,\n" +
            "    \"EndTimestamp\" timestamp without time zone,\n" +
            "    CONSTRAINT \"SessionPrimaryKey\" PRIMARY KEY (\"SessionID\")\n" +
            ")\n" +
            "WITH (\n" +
            "    OIDS = FALSE\n" +
            ");\n" +
            "ALTER TABLE \"Session\"\n" +
            "    OWNER to " + getDatabaseUserName() + ";";

    public static String INSERT_NEW_SESSION = "INSERT INTO \"Session\" (\"SessionID\", \"StartTimestamp\")\n" +
            "VALUES (%S, ?)\n" +
            "ON CONFLICT (\"SessionID\") DO NOTHING;";

    public static String SESSION_FOR_TIMESTAMP = "SELECT \"SessionID\" FROM \"Session\"\n" +
            "WHERE \"StartTimestamp\" = ?;";

    public static String ADD_MEASUREMENT = "INSERT INTO \"Measurement\" (\"SessionID\", \"Timestamp\", \"Temperature\")\n" +
            "VALUES (?, ?, ?);";

    public static String CREATE_TABLE_MEASUREMENT = "CREATE TABLE public.\"Measurement\"\n" +
            "(\n" +
            "    \"MeasurementID\" serial NOT NULL,\n" +
            "    \"SessionID\" integer,\n" +
            "    \"Timestamp\" timestamp with time zone NOT NULL,\n" +
            "    \"Temperature\" real NOT NULL,\n" +
            "    CONSTRAINT \"MeasurementPrimaryKey\" PRIMARY KEY (\"MeasurementID\"),\n" +
            "    CONSTRAINT \"SessionForeignKey\" FOREIGN KEY (\"SessionID\")\n" +
            "        REFERENCES public.\"Session\" (\"SessionID\") MATCH SIMPLE\n" +
            "        ON UPDATE NO ACTION\n" +
            "        ON DELETE NO ACTION\n" +
            ")\n" +
            "WITH (\n" +
            "    OIDS = FALSE\n" +
            ");\n" +
            "ALTER TABLE public.\"Measurement\"\n" +
            "    OWNER to " + getDatabaseUserName() + ";";

    static String TABLE_EXISTS = "SELECT EXISTS (\n" +
            "   SELECT 1 \n" +
            "   FROM   pg_tables\n" +
            "   WHERE  schemaname = 'public'\n" +
            "   AND    tablename = '%s'\n" +
            "   );";

}

package leberechtc.tempih;

import leberechtc.tempih.exceptions.SystemException;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static leberechtc.tempih.Configuration.getCurrentSession;
import static leberechtc.tempih.Configuration.getSensorId;

public class SensorInterface {

    private final static Logger logger = Logger.getLogger(SensorInterface.class);

    private final Path DEFAULT_DEVICE_PATH = Paths.get("/sys/bus/w1/devices/");
    private final String DEFAUT_READ_FILE = "w1_slave";

    private static double lastSimulatedMeasurement = 11.0;

    private final Pattern temperatureReadPattern = Pattern.compile("t=(\\d+)");

    private Path rawProbeReadFile;

    public SensorInterface() {
        loadModules();
        initializePath();
    }

    private void initializePath() {
        rawProbeReadFile = DEFAULT_DEVICE_PATH.resolve(getSensorId()).resolve(DEFAUT_READ_FILE);
    }

    private void loadModules() {
        executeCommand("modprobe w1-gpio");
        executeCommand("modprobe w1-therm");
    }

    private List<String> readRawLines() {
        try {
            return Files.readAllLines(rawProbeReadFile);
        } catch (IOException e) {
            throw new SystemException("Unable to read raw probe file: " + rawProbeReadFile, e);
        }
    }

    public Measurement measureTemperature() {
        // read raw
        List<String> readLines = readRawLines();
        Iterator<String> iterator = readLines.iterator();
        // check if read was valid by testing if the first line ends with "YES"
        int maxTries = 5;
        while (!iterator.next().endsWith("YES") && maxTries >= 0) {
            iterator = readRawLines().iterator();
            maxTries--;
        }
        if (maxTries == 0) {
            throw new SystemException("Unable to get valid sensor reads from " + rawProbeReadFile + " after 5 tries.");
        }
        // get next line and read temperature
        String temperatureLine = iterator.next();
        Matcher temperatureMatcher = temperatureReadPattern.matcher(temperatureLine);
        if (temperatureMatcher.find()) {
            double rawTemperature = Double.valueOf(temperatureMatcher.group(1));
            double actualTemperature = rawTemperature / 1000.0;
            final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            return new Measurement(getCurrentSession(), timestamp, actualTemperature);
        }
        return null;
    }

    public static Measurement simulateMeasurement() {
        double random;
        if (Math.random() > 0.5) {
            random = Math.random();
        } else {
            random = -Math.random();
        }
        lastSimulatedMeasurement += random;
        return new Measurement(0, new Timestamp(System.currentTimeMillis()), lastSimulatedMeasurement);
    }

    private static String executeCommand(String command) {
        StringBuilder output = new StringBuilder();
        Process process;
        try {
            process = Runtime.getRuntime().exec(command);
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append(System.lineSeparator());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

}

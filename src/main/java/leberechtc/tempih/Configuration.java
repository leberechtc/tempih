package leberechtc.tempih;

import leberechtc.tempih.exceptions.SystemException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Configuration {

    private final static Logger logger = Logger.getLogger(Configuration.class);
    private static final Pattern keyValuePattern = Pattern.compile("^(\\w+)\\s*=\\s*(.+)\\s*$");
    private static Configuration instance;

    private boolean csv = false;
    private String databaseName = "tempih";
    private String databaseUserName = "pi";
    private String databaseUserPassword = "raspberry";
    private String csvFile = "data/session";
    private String sensorId = "28-0316b1512bff";
    private int measurementInterval = 10;
    private int session = 0;
    private double minimalTemperature = 20;
    private double maximalTemperature = 30;
    private boolean cooling = false;

    public static Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public static void initializeFromConfigFile(Path configFile) {
        logger.info("Initializing from config file: " + configFile);
        try {
            List<String> lines = Files.readAllLines(configFile);
            for (String line : lines) {
                if (line.startsWith("#")) {
                    continue;
                }
                Matcher keyValueMatcher = keyValuePattern.matcher(line);
                if (keyValueMatcher.matches()) {
                    String key = keyValueMatcher.group(1);
                    String value = keyValueMatcher.group(2);
                    switch (key) {
                        case "INTERFACE": {
                            if (value.equalsIgnoreCase("DATABASE")) {
                                logger.debug("Recording to database.");
                                getInstance().csv = false;
                            } else {
                                logger.debug("Recording to csv file.");
                                getInstance().csv = true;
                            }
                            break;
                        }
                        case "DATABASE_NAME": {
                            logger.debug("Set database name to " + value);
                            getInstance().databaseName = value;
                            break;
                        }
                        case "DATABASE_USER_NAME": {
                            logger.debug("Set database user name to " + value);
                            getInstance().databaseUserName = value;
                            break;
                        }
                        case "DATABASE_USER_PASSWORD": {
                            logger.debug("Set database password (duh)");
                            getInstance().databaseUserPassword = value;
                            break;
                        }
                        case "CSV_FILE": {
                            logger.debug("Recording to file " + value);
                            getInstance().csvFile = value;
                            break;
                        }
                        case "SENSOR_ID": {
                            logger.debug("Set sensor id to " + value);
                            getInstance().sensorId = value;
                            break;
                        }
                        case "MEASUREMENT_INTERVAL": {
                            logger.debug("Set measurement interval to " + value + " seconds");
                            getInstance().measurementInterval = Integer.parseInt(value);
                            break;
                        }
                        case "CURRENT_SESSION": {
                            int session = Integer.parseInt(value);
                            logger.debug(((session == 0) ? "Beginning new session." : "Continuing session " + value));
                            getInstance().session = session;
                            break;
                        }
                        case "MINIMAL_TEMPERATURE": {
                            logger.debug("Set minimal temperature to " + value + " °C");
                            getInstance().minimalTemperature = Double.parseDouble(value);
                            break;
                        }
                        case "MAXIMAL_TEMPERATURE": {
                            logger.debug("Set maximal temperature to " + value + " °C");
                            getInstance().maximalTemperature = Double.parseDouble(value);
                            break;
                        }
                        case "ACTIVITY": {
                            if (value.equalsIgnoreCase("cool")) {
                                logger.debug("Device cools.");
                                getInstance().cooling = true;
                            } else {
                                logger.debug("Device heats.");
                                getInstance().cooling = false;
                            }
                            break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new SystemException("Unable to read config file from path: " + configFile, e);
        }
    }

    public static String getDatabaseName() {
        return getInstance().databaseName;
    }

    public static String getDatabaseUserName() {
        return getInstance().databaseUserName;
    }

    public static String getDatabaseUserPassword() {
        return getInstance().databaseUserPassword;
    }

    public static String getSensorId() {
        return getInstance().sensorId;
    }

    public static int getMeasurementInterval() {
        return getInstance().measurementInterval;
    }

    public static int getCurrentSession() {
        return getInstance().session;
    }

    public static void setCurrentSession(int session) {
        logger.debug("Set current session identifier to " + session + ".");
        getInstance().session = session;
    }

    public static double getMinimalTemperature() {
        return getInstance().minimalTemperature;
    }

    public static double getMaximalTemperature() {
        return getInstance().maximalTemperature;
    }

    public static boolean isCooling() {
        return getInstance().cooling;
    }

    public static boolean isCsv() {
        return getInstance().csv;
    }

    public static String getCsvFile() {
        return getInstance().csvFile;
    }
}

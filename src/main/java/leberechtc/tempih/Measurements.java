package leberechtc.tempih;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class Measurements {

    private TreeSet<Measurement> measurements;

    public Measurements() {
        this.measurements = new TreeSet<>(Comparator.comparing(Measurement::getTimestamp));
    }

    public TreeSet<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(TreeSet<Measurement> measurements) {
        this.measurements = measurements;
    }

    public Measurement getLeatestMeasurement() {
        return this.measurements.last();
    }

    public void add(Measurement measurement) {
        measurements.add(measurement);
    }

    public double getAcceleration(int numberOfMeasurements) {
        final int size = measurements.size();
        // 0 or 1 one measurement only
        if (size < 2) {
            return 0.0;
        }
        // no more iterations than measurements
        if (size < numberOfMeasurements) {
            numberOfMeasurements = size;
        }
        // remember for division
        final double divisor = numberOfMeasurements - 1;
        // summing differences
        double differenceSum = 0.0;
        Iterator<Measurement> iterator = this.measurements.descendingIterator();
        Measurement last = iterator.next();
        while (numberOfMeasurements >= 0 && iterator.hasNext()) {
            Measurement current = iterator.next();
            differenceSum += last.getTemperature() - current.getTemperature();
            last = current;
            numberOfMeasurements--;
        }
        return differenceSum / divisor;
    }

}

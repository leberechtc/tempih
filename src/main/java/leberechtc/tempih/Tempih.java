package leberechtc.tempih;

import leberechtc.tempih.exceptions.SystemException;
import org.apache.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static leberechtc.tempih.TemperatureObserver.TemperatureState;
import static leberechtc.tempih.TemperatureObserver.TemperatureState.*;

public class Tempih {

    private final static Logger logger = Logger.getLogger(Tempih.class);

    private static boolean simulateMeasurements = false;

    private static Recorder recorder;
    private static SensorInterface sensorInterface;
    private static TemperatureObserver temperatureObserver;
    private static RelayInterface relayInterface;

    private static TemperatureState previousTemperatureState = JUST_RIGHT;

    public static void main(String[] args) {
        // read config file from given path
        if (args == null) {
            throw new SystemException("Please enter the location of the configuration file to read.");
        }
        Path configFile = Paths.get(args[0]);
        Configuration.initializeFromConfigFile(configFile);
        // setup database
        if (Configuration.isCsv()) {
            recorder = new CSVInterface();
        } else {
            recorder = new DatabaseInterface();
        }
        // setup temperature sensor
        if (!simulateMeasurements) {
            sensorInterface = new SensorInterface();
        }
        // initialize temperature observer
        temperatureObserver = new TemperatureObserver(Configuration.getMinimalTemperature(), Configuration.getMaximalTemperature());
        // initialize relay interface
        if (!simulateMeasurements) {
            relayInterface = new RelayInterface();
        }
        // schedule execution
        final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(Tempih::measure, 0, Configuration.getMeasurementInterval(), TimeUnit.SECONDS);
    }

    private static void measure() {
        Measurement measurement;
        if (simulateMeasurements) {
            measurement = SensorInterface.simulateMeasurement();
        } else {
            measurement = sensorInterface.measureTemperature();
        }
        if (measurement != null) {
            acceptMeasurement(measurement);
        } else {
            logger.info("Current Temperature: unable to read temperature");
        }
    }

    private static void acceptMeasurement(Measurement measurement) {
        logger.info("Current Temperature: " + measurement);
        recorder.addMeasurement(measurement);
        handleTemperatureControl(temperatureObserver.checkMeasurement(measurement));
    }

    private static void handleTemperatureControl(TemperatureState newTemperatureState) {
        if (Configuration.isCooling()) {
            tooHighSwitchOn(newTemperatureState);
            tooLowSwitchOff(newTemperatureState);
        } else {
            tooHighSwitchOff(newTemperatureState);
            tooLowSwitchOn(newTemperatureState);
        }
        previousTemperatureState = newTemperatureState;
    }

    private static void tooHighSwitchOff(TemperatureState newTemperatureState) {
        if ((previousTemperatureState == JUST_RIGHT) && (newTemperatureState == TOO_HIGH)) {
            logger.info("Temperature too high, switching relay off.");
            if (!simulateMeasurements) {
                relayInterface.switchOff();
            }
        }
    }

    private static void tooHighSwitchOn(TemperatureState newTemperatureState) {
        if ((previousTemperatureState == JUST_RIGHT) && (newTemperatureState == TOO_HIGH)) {
            logger.info("Temperature too high, switching relay on.");
            if (!simulateMeasurements) {
                relayInterface.switchOn();
            }
        }
    }

    private static void tooLowSwitchOff(TemperatureState newTemperatureState) {
        if ((previousTemperatureState == JUST_RIGHT) && (newTemperatureState == TOO_LOW)) {
            logger.info("Temperature too low, switching relay off.");
            if (!simulateMeasurements) {
                relayInterface.switchOff();
            }
        }
    }

    private static void tooLowSwitchOn(TemperatureState newTemperatureState) {
        if ((previousTemperatureState == JUST_RIGHT) && (newTemperatureState == TOO_LOW)) {
            logger.info("Temperature too low, switching relay on.");
            if (!simulateMeasurements) {
                relayInterface.switchOn();
            }
        }
    }

}

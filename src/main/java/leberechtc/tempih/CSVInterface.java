package leberechtc.tempih;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static leberechtc.tempih.Configuration.getCurrentSession;

/**
 * @author cl
 */
public class CSVInterface implements Recorder {

    private final static Logger logger = Logger.getLogger(CSVInterface.class);
    private final static DateFormat ISO_8601_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
    private static final DecimalFormat temperatureFormat = new DecimalFormat("00.00");
    private Path file;

    public CSVInterface() {
        setupFile();
    }

    private void setupFile() {
        String fileLocation = Configuration.getCsvFile() + "_" + getCurrentSession() + ".csv";
        Path filePath = Paths.get(fileLocation);
        Recorder.createDirectories(filePath.getParent());
        file = Recorder.createFile(filePath);
        logger.debug("Writing to file" + file.toString());
    }

    @Override
    public void addMeasurement(Measurement measurement) {
        Date date = new Date(measurement.getTimestamp().getTime());
        String line = ISO_8601_DATE_TIME.format(date) + "," + temperatureFormat.format(measurement.getTemperature()) + System.lineSeparator();
        try {
            Files.write(file, line.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            logger.error("Unable to write measurement " + measurement);
        }
    }
}

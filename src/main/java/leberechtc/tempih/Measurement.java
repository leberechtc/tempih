package leberechtc.tempih;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class Measurement {

    private static final DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DecimalFormat temperatureFormat = new DecimalFormat("00.00");

    private int session;
    private Timestamp timestamp;
    private double temperature;

    public Measurement(int session, Timestamp timestamp, double temperature) {
        this.session = session;
        this.timestamp = timestamp;
        this.temperature = temperature;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return timestampFormat.format(timestamp) + " : " + temperatureFormat.format(temperature) + " °C";
    }
}

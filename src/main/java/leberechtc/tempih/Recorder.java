package leberechtc.tempih;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author cl
 */
public interface Recorder {

    void addMeasurement(Measurement measurement);

    static void createDirectories(Path currentVariationSetPath) {
        if (!Files.exists(currentVariationSetPath)) {
            try {
                Files.createDirectories(currentVariationSetPath);
            } catch (IOException e) {
                throw new UncheckedIOException("Unable to create directory " + currentVariationSetPath + " for current simulation variation.", e);
            }
        }
    }

    static Path createFile(Path path, String fileName) {
        return createFile(path.resolve(fileName));
    }

    static Path createFile(Path path) {
        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                throw new UncheckedIOException("Unable to create file " + path + " for current simulation variation.", e);
            }
        }
        return path;
    }

}
